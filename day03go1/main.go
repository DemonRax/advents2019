package main

import (
	"fmt"
	"sort"
	"strconv"
	"strings"

	"gitlab.com/DemonRax/advents2019/util"
)

func main() {
	input := util.ReadFile("input.txt")
	fmt.Println(cross(input))
}

func cross(ss []string) int {
	field := make(map[int]map[int]map[int]struct{})
	for i, s := range ss {
		draw(strings.Split(s, ","), i+1, field)
	}
	return manhattan(field, len(ss))
}

func draw(steps []string, num int, field map[int]map[int]map[int]struct{}) {
	var x, y int
	for _, s := range steps {
		dx, dy := parse(s)
		fill(x, y, dx, dy, num, field)
		x += dx
		y += dy
	}
}

func fill(x, y, dx, dy, num int, field map[int]map[int]map[int]struct{}) {
	x1 := x + dx
	y1 := y + dy

	if dx != 0 {
		if x1 < x {
			x--
			x, x1 = x1, x
		} else {
			x++
		}
	}

	if dy != 0 {
		if y1 < y {
			y--
			y, y1 = y1, y
		} else {
			y++
		}
	}

	for i := x; i <= x1; i++ {
		for j := y; j <= y1; j++ {
			if len(field[i]) == 0 {
				field[i] = make(map[int]map[int]struct{})
			}
			if len(field[i][j]) == 0 {
				field[i][j] = make(map[int]struct{})
			}
			field[i][j][num] = struct{}{}
		}
	}
}

func manhattan(field map[int]map[int]map[int]struct{}, target int) int {
	var crosses []int
	for i, f := range field {
		for j, v := range f {
			if len(v) == target {
				crosses = append(crosses, util.Abs(i)+util.Abs(j))
			}
		}
	}
	if len(crosses) == 0 {
		return 0
	}
	sort.Ints(crosses)
	return crosses[0]
}

func parse(s string) (dx, dy int) {
	value, _ := strconv.Atoi(s[1:])
	switch s[0] {
	case 'R':
		return value, 0
	case 'L':
		return -value, 0
	case 'U':
		return 0, value
	case 'D':
		return 0, -value
	}
	return 0, 0
}
