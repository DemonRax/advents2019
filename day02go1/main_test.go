package main

import "testing"

func Test_code(t *testing.T) {
	for _, test := range []struct {
		in,
		want string
	}{
		{
			in:   "1,0,0,0,99",
			want: "2,0,0,0,99",
		},
		{
			in:   "2,3,0,3,99",
			want: "2,3,0,6,99",
		},
		{
			in:   "2,4,4,5,99,0",
			want: "2,4,4,5,99,9801",
		},
		{
			in:   "1,1,1,4,99,5,6,0,99",
			want: "30,1,1,4,2,5,6,0,99",
		},
	} {
		t.Run(test.in, func(t *testing.T) {
			if got := intcode(test.in, nil); got != test.want {
				t.Errorf("got = %s, want %s", got, test.want)
			}
		})
	}
}
