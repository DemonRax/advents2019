package main

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/DemonRax/advents2019/util"
)

func main() {
	input := util.ReadFile("input.txt")
	fmt.Println(intcode(input[0], map[int]int{1: 12, 2: 2}))
}

func intcode(s string, replace map[int]int) string {
	codes := strings.Split(s, ",")
	ints := make([]int, len(codes))
	for i, c := range codes {
		ints[i], _ = strconv.Atoi(c)
	}
	for i, v := range replace {
		ints[i] = v
	}
	i := 0
	for {
		if ints[i] == 99 {
			break
		}
		if ints[i] == 1 {
			ints[ints[i+3]] = ints[ints[i+1]] + ints[ints[i+2]]
		} else if ints[i] == 2 {
			ints[ints[i+3]] = ints[ints[i+1]] * ints[ints[i+2]]
		}
		i += 4
	}
	for i := range ints {
		codes[i] = strconv.Itoa(ints[i])
	}
	return strings.Join(codes, ",")
}
