package main

import (
	"fmt"

	"gitlab.com/DemonRax/advents2019/util"
)

func main() {
	input := util.ReadFile("input.txt")
	fmt.Println(layers(input[0], 25, 6))
}

func layers(s string, width, height int) int {
	var res, c int
	min := -1
	for c < len(s) {
		var z, o, t int
		for x := 0; x < width; x++ {
			for y := 0; y < height; y++ {
				switch s[c] {
				case '0':
					z++
				case '1':
					o++
				case '2':
					t++
				}
				c++
			}
		}
		if min < 0 || min > z {
			min = z
			res = o*t
		}
	}
	return res
}
