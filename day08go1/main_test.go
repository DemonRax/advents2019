package main

import (
	"testing"
)

func Test_code(t *testing.T) {
	for _, test := range []struct {
		in   string
		width,
		height,
		want int
	}{
		{
			in:   "123456789012",
			width: 3,
			height: 2,
			want: 1,
		},
		{
			in:   "123456789512123456789012123456789012123456789012",
			width: 3,
			height: 4,
			want: 4,
		},
		{
			in:   "123456789012123456789012123456789012123456789512",
			width: 3,
			height: 4,
			want: 4,
		},
	} {
		t.Run(test.in, func(t *testing.T) {
			if got := layers(test.in, test.width, test.height); got != test.want {
				t.Errorf("got = %d, want %d", got, test.want)
			}
		})
	}
}
