package main

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/DemonRax/advents2019/util"
)

func main() {
	input := util.ReadFile("input.txt")
	fmt.Println(steps(input))
}

func steps(ss []string) int {
	return cross(ss)
}

func cross(ss []string) int {
	field := make(map[int]map[int]map[int]struct{})
	for i, s := range ss {
		draw(strings.Split(s, ","), i+1, field)
	}
	xs, ys := crosses(field, len(ss))
	paths := make([]int, len(xs))
	fmt.Println(xs, ys)
	for k := 0; k < len(xs); k++ {
		for _, s := range ss {
			var x, y, path int
			for _, e := range strings.Split(s, ",") {
				dx, dy := parse(e)
				x0, x1, y0, y1 := moveCoords(x, y, dx, dy)
				for i := x0; i <= x1; i++ {
					if x0 != x1 {
						x = i
						path++
					}
					for j := y0; j <= y1; j++ {
						if y0 != y1 {
							y = j
							path++
						}
						fmt.Println(k, x, y, path)
						if x == xs[k] && y == ys[k] {
							//	paths[k] = paths[k] + path
						}
					}
				}
				//path += abs(dx) + abs(dy)
			}
		}
	}
	fmt.Println(paths)
	return 0
	//short := 2147483647
}

func draw(steps []string, num int, field map[int]map[int]map[int]struct{}) {
	var x, y int
	for _, s := range steps {
		dx, dy := parse(s)
		fill(x, y, dx, dy, num, field)
		x += dx
		y += dy
	}
}

func moveCoords(x, y, dx, dy int) (x0, x1, y0, y1 int) {
	x1 = x + dx
	y1 = y + dy

	if dx != 0 {
		if x1 < x {
			x--
			x, x1 = x1, x
		} else {
			x++
		}
	}

	if dy != 0 {
		if y1 < y {
			y--
			y, y1 = y1, y
		} else {
			y++
		}
	}
	return x, x1, y, y1
}

func fill(x, y, dx, dy, num int, field map[int]map[int]map[int]struct{}) {
	x, x1, y, y1 := moveCoords(x, y, dx, dy)
	for i := x; i <= x1; i++ {
		for j := y; j <= y1; j++ {
			if len(field[i]) == 0 {
				field[i] = make(map[int]map[int]struct{})
			}
			if len(field[i][j]) == 0 {
				field[i][j] = make(map[int]struct{})
			}
			field[i][j][num] = struct{}{}
		}
	}
}

func crosses(field map[int]map[int]map[int]struct{}, target int) ([]int, []int) {
	var xs, ys []int
	for i, f := range field {
		for j, v := range f {
			if len(v) == target {
				xs = append(xs, i)
				ys = append(ys, j)
			}
		}
	}
	return xs, ys
}

func parse(s string) (dx, dy int) {
	value, _ := strconv.Atoi(s[1:])
	switch s[0] {
	case 'R':
		return value, 0
	case 'L':
		return -value, 0
	case 'U':
		return 0, value
	case 'D':
		return 0, -value
	}
	return 0, 0
}
