package main

import (
	"strconv"
	"testing"
)

func Test_code(t *testing.T) {
	for _, test := range []struct {
		in,
		want int
	}{
		{
			in:   122345,
			want: 1,
		},
		{
			in:   111123,
			want: 0,
		},
		{
			in:   135679,
			want: 0,
		},
		{
			in:   111111,
			want: 0,
		},
		{
			in:   223450,
			want: 0,
		},
		{
			in:   123789,
			want: 0,
		},
		{
			in:   112233,
			want: 1,
		},
		{
			in:   123444,
			want: 0,
		},
		{
			in:   111122,
			want: 1,
		},
	} {
		t.Run(strconv.Itoa(test.in), func(t *testing.T) {
			if got := valid(test.in); got != test.want {
				t.Errorf("got = %d, want %d", got, test.want)
			}
		})
	}
}
