package util

import (
	"sort"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func Test_Permutations(t *testing.T) {
	for _, test := range []struct {
		in   string
		want []string
	}{
		{
			in:   "A",
			want: []string{"A"},
		},
		{
			in:   "AB",
			want: []string{"BA", "AB"},
		},
		{
			in:   "ABC",
			want: []string{"BAC", "BCA", "ACB", "ABC", "CAB", "CBA"},
		},
	} {
		t.Run(test.in, func(t *testing.T) {
			got := Permutations(test.in)
			sort.Strings(test.want)
			sort.Strings(got)
			if diff := cmp.Diff(test.want, got); diff != "" {
				t.Errorf("diff (want/got): %s", diff)
			}
		})
	}
}
