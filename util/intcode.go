package util

import (
	"math"
	"strconv"
	"strings"
)

type IntCode struct {
	input     int
	codes     []int
	cursor    int
	mem       int
	running   bool
	phaseUsed bool
	base      int
}

func (ic *IntCode) Run(stopAtMemWrite bool) {
	for {
		if !ic.running ||
			ic.Step() && stopAtMemWrite {
			break
		}
	}
}

func (ic *IntCode) SetMemory(mem int) {
	ic.mem = mem
}

func (ic *IntCode) IsRunning() bool {
	return ic.running
}

func (ic *IntCode) Memory() int {
	return ic.mem
}

func (ic *IntCode) readValue(i, mode int) int {
	switch mode {
	case 0:
		return ic.codes[ic.codes[i]]
	case 1:
		return ic.codes[i]
	case 2:
		return ic.codes[ic.codes[i]+ic.base]
	default:
		return 0
	}
}

func (ic *IntCode) writePos(i, mode int) int {
	switch mode {
	case 0:
		return ic.codes[i]
	case 1:
		panic("write in mode 1")
	case 2:
		return ic.codes[i]+ic.base
	default:
		return 0
	}
}

func (ic *IntCode) Step() (memWrite bool) {
	if !ic.running {
		return
	}
	code, mode1, mode2, mode3 := ic.codes[ic.cursor], 0, 0, 0
	if code == 99 {
		ic.running = false
		return
	}
	if code > 99 {
		code, mode1, mode2, mode3 = params(code)
	}

	value1 := ic.readValue(ic.cursor+1, mode1)
	value2 := ic.readValue(ic.cursor+2, mode2)
	pos3 := ic.writePos(ic.cursor+3, mode3)
	switch code {
	case 1:
		ic.codes[pos3] = value1 + value2
		ic.cursor += 4
	case 2:
		ic.codes[pos3] = value1 * value2
		ic.cursor += 4
	case 3:
		ic.codes[ic.writePos(ic.cursor+1, mode1)] = ic.mem
		if !ic.phaseUsed {
			ic.phaseUsed = true
			ic.mem = ic.input
		}
		ic.cursor += 2
	case 4:
		ic.mem = value1
		ic.cursor += 2
		return true
	case 5:
		if value1 != 0 {
			ic.cursor = value2
		} else {
			ic.cursor += 3
		}
	case 6:
		if value1 == 0 {
			ic.cursor = value2
		} else {
			ic.cursor += 3
		}
	case 7:
		if value1 < value2 {
			ic.codes[pos3] = 1
		} else {
			ic.codes[pos3] = 0
		}
		ic.cursor += 4
	case 8:
		if value1 == value2 {
			ic.codes[pos3] = 1
		} else {
			ic.codes[pos3] = 0
		}
		ic.cursor += 4
	case 9:
		ic.base += value1
		ic.cursor += 2
	}
	return false
}

func NewComputer(program string, phase, input int) *IntCode {
	codes := strings.Split(program, ",")
	ints := make([]int, 100000)
	for i, c := range codes {
		ints[i], _ = strconv.Atoi(c)
	}
	return &IntCode{
		mem:     phase,
		input:   input,
		codes:   ints,
		cursor:  0,
		running: true,
	}
}

func params(code int) (op, mode1, mode2, mode3 int) {
	return digit(code, 1), digit(code, 3), digit(code, 4), digit(code, 5)
}

func digit(num, place int) int {
	r := num % int(math.Pow(10, float64(place)))
	return r / int(math.Pow(10, float64(place-1)))
}
