package main

import (
	"fmt"
	"sort"

	"gitlab.com/DemonRax/advents2019/util"
)

func main() {
	input := util.ReadFile("input.txt")
	fmt.Println(highest(input[0]))
}

func highest(s string) int {
	phases := map[rune]int{
		'A': 0,
		'B': 1,
		'C': 2,
		'D': 3,
		'E': 4,
	}
	var res []int
	for _, p := range util.Permutations("ABCDE") {
		mem := 0
		for _, r := range p {
			ic := util.NewComputer(s, phases[r], mem)
			ic.Run(false)
			mem = ic.Memory()
		}
		res = append(res, mem)
	}
	sort.Ints(res)
	return res[len(res)-1]
}
