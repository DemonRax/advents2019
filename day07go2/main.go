package main

import (
	"fmt"
	"sort"

	"gitlab.com/DemonRax/advents2019/util"
)

func main() {
	input := util.ReadFile("input.txt")
	fmt.Println(highest(input[0]))
}

func highest(s string) int {
	phases := map[rune]int{
		'A': 5,
		'B': 6,
		'C': 7,
		'D': 8,
		'E': 9,
	}
	var res []int
	for _, p := range util.Permutations("ABCDE") {
		mem := 0
		pcs := map[rune]*util.IntCode{}
		for _, r := range p {
			ic := util.NewComputer(s, phases[r], mem)
			ic.Step()
			pcs[r] = ic
		}
		for {
			for _, r := range p {
				pcs[r].SetMemory(mem)
				pcs[r].Run(true)
				mem = pcs[r].Memory()
			}
			if !pcs[rune(p[len(p)-1])].IsRunning() {
				break
			}
		}
		res = append(res, mem)
	}
	sort.Ints(res)
	return res[len(res)-1]
}
