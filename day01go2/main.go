package main

import (
	"fmt"
	"strconv"

	"gitlab.com/DemonRax/advents2019/util"
)

func main() {
	input := util.ReadFile("input.txt")
	result := 0
	for _, in := range input {
		result += fuels(in)
	}
	fmt.Println(result)
}

func fuels(s string) int {
	var r int
	f, _ := strconv.Atoi(s)
	for {
		f = f/3 - 2
		if f <= 0 {
			break
		}
		r += f
	}
	return r
}
