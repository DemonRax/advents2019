package main

import (
	"fmt"
	"strconv"

	"gitlab.com/DemonRax/advents2019/util"
)

func main() {
	input := util.ReadFile("input.txt")
	fmt.Println(validPasswords(input[0], input[1]))
}

func validPasswords(start, end string) int {
	s, _ := strconv.Atoi(start)
	e, _ := strconv.Atoi(end)

	var count int
	for i := s; i <= e; i++ {
		count += valid(i)
	}
	return count
}

func valid(num int) int {
	n := strconv.Itoa(num)
	var double int
	for i := 0; i < 5; i++ {
		if n[i] > n[i+1] {
			return 0
		}
		if n[i] == n[i+1] {
			double = 1
		}
	}
	return double
}
