package main

import (
	"testing"
)

type testPainter struct {
	cursor       int
	instructions []int
}

func (tp *testPainter) paint(in int) (color, dir int) {
	if tp.cursor >= len(tp.instructions) {
		return
	}
	tp.cursor = tp.cursor + 2
	return tp.instructions[tp.cursor-2], tp.instructions[tp.cursor-1]
}

func (tp *testPainter) done() bool {
	return tp.cursor >= len(tp.instructions)
}

var _ robotPainter = (*testPainter)(nil)

func Test_code(t *testing.T) {
	for _, test := range []struct {
		in   []int
		want int
	}{
		{
			in:   []int{1, 0, 0, 0, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0},
			want: 6,
		},
	} {
		t.Run("", func(t *testing.T) {
			if got := paintAtLeastOnce(&testPainter{instructions: test.in}); got != test.want {
				t.Errorf("got = %d, want %d", got, test.want)
			}
		})
	}
}
