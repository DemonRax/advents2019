package main

import (
	"fmt"

	"gitlab.com/DemonRax/advents2019/util"
)

func main() {
	input := util.ReadFile("input.txt")
	ic := util.NewComputer(input[0], 0, 0)
	ic.Step()
	fmt.Println(paintAtLeastOnce(intCodePainter{ic: ic}))
}

type robotPainter interface {
	paint(in int) (color, direction int)
	done() bool
}

type intCodePainter struct {
	ic *util.IntCode
}

func (icp intCodePainter) paint(in int) (color, directon int) {
	icp.ic.SetMemory(in)
	icp.ic.Run(true)
	color = icp.ic.Memory()
	icp.ic.Run(true)
	directon = icp.ic.Memory()
	return
}

func (icp intCodePainter) done() bool {
	return !icp.ic.IsRunning()
}

var _ robotPainter = intCodePainter{}

type coord struct {
	x, y int
}

func paintAtLeastOnce(painter robotPainter) int {
	grid := make(map[coord]int)
	robot := coord{}
	dir := up
	count := 0
	for !painter.done() {
		color, turn := painter.paint(grid[robot])
		if _, ok := grid[robot]; !ok {
			count++
		}
		grid[robot] = color
		robot = move(robot, dir, turn)
		dir = dir.turn(turn)
	}
	return len(grid)
}

func move(robot coord, dir dir, turn int) coord {
	switch dir.turn(turn) {
	case up:
		return coord{robot.x, robot.y - 1}
	case left:
		return coord{robot.x - 1, robot.y}
	case down:
		return coord{robot.x, robot.y + 1}
	case right:
		return coord{robot.x + 1, robot.y}
	}
	return robot
}

type dir string

const (
	up    dir = "^"
	down  dir = "v"
	left  dir = "<"
	right dir = ">"
)

func (d dir) turn(where int) dir {
	switch {
	case d == up && where == 0:
		return left
	case d == up && where == 1:
		return right
	case d == left && where == 0:
		return down
	case d == left && where == 1:
		return up
	case d == down && where == 0:
		return right
	case d == down && where == 1:
		return left
	case d == right && where == 0:
		return up
	case d == right && where == 1:
		return down
	}
	return d
}
