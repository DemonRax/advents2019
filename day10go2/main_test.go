package main

import (
	"testing"

	"gitlab.com/DemonRax/advents2019/util"
)

func Test_code(t *testing.T) {
	for _, test := range []struct {
		in   string
		want rock
	}{
		{
			in: `.#..##.###...#######
##.############..##.
.#.######.########.#
.###.#######.####.#.
#####.##.#.##.###.##
..#####..#.#########
####################
#.####....###.#.#.##
##.#################
#####.##.###..####..
..######..##.#######
####.##.####...##..#
.#####..#.######.###
##...#.##########...
#.##########.#######
.####.#.###.###.#.##
....##.##.###..#####
.#.#.###########.###
#.#.#.#####.####.###
###.##.####.##.#..##
`,
			want: rock{8, 2, 0},
		},
	} {
		t.Run("", func(t *testing.T) {
			if got := coord200(util.ReadString(test.in)); got.x != test.want.x || got.y != test.want.y {
				t.Errorf("got = %v, want %v", got, test.want)
			}
		})
	}
}

func Test_maxRocks(t *testing.T) {
	for _, test := range []struct {
		in   string
		want int
	}{
		{
			in:   `.###`,
			want: 2,
		},
		{
			in: `.###
..#.
..#.`,
			want: 4,
		},
		{
			in: `.#..#
.....
#####
....#
...##
`,
			want: 8,
		},
		{
			in: `......#.#.
#..#.#....
..#######.
.#.#.###..
.#..#.....
..#....#.#
#..#....#.
.##.#..###
##...#..#.
.#....####
`,
			want: 33,
		},
		{
			in: `#.#...#.#.
.###....#.
.#....#...
##.#.#.#.#
....#.#.#.
.##..###.#
..#...##..
..##....##
......#...
.####.###.
`,
			want: 35,
		},
		{
			in: `.#..#..###
####.###.#
....###.#.
..###.##.#
##.##.#.#.
....###..#
..#.#..#.#
#..#.#.###
.##...##.#
.....#.#..
`,
			want: 41,
		},
		{
			in: `.#..##.###...#######
##.############..##.
.#.######.########.#
.###.#######.####.#.
#####.##.#.##.###.##
..#####..#.#########
####################
#.####....###.#.#.##
##.#################
#####.##.###..####..
..######..##.#######
####.##.####...##..#
.#####..#.######.###
##...#.##########...
#.##########.#######
.####.#.###.###.#.##
....##.##.###..#####
.#.#.###########.###
#.#.#.#####.####.###
###.##.####.##.#..##
`,
			want: 210,
		},
	} {
		t.Run(test.in[:4], func(t *testing.T) {
			if got, _ := station(util.ReadString(test.in)); got != test.want {
				t.Errorf("got = %d, want %d", got, test.want)
			}
		})
	}
}

func Test_between(t *testing.T) {
	for _, test := range []struct {
		r, r1, r2 rock
		want      bool
	}{
		{
			r:    rock{5, 6, 0},
			r1:   rock{1, 1, 0},
			r2:   rock{10, 10, 0},
			want: true,
		},
		{
			r:    rock{5, 6, 0},
			r1:   rock{5, 6, 0},
			r2:   rock{10, 10, 0},
			want: false,
		},
		{
			r:    rock{5, 6, 0},
			r1:   rock{10, 10, 0},
			r2:   rock{10, 10, 0},
			want: false,
		},
		{
			r:    rock{10, 10, 0},
			r1:   rock{10, 10, 0},
			r2:   rock{10, 10, 0},
			want: false,
		},
		{
			r:    rock{10, 10, 0},
			r1:   rock{5, 6, 0},
			r2:   rock{10, 10, 0},
			want: false,
		},
		{
			r:    rock{5, 6, 0},
			r1:   rock{10, 10, 0},
			r2:   rock{1, 1, 0},
			want: true,
		},
		{
			r:    rock{1, 1, 0},
			r1:   rock{10, 10, 0},
			r2:   rock{5, 6, 0},
			want: false,
		},
		{
			r:    rock{5, 6, 0},
			r1:   rock{5, 1, 0},
			r2:   rock{5, 10, 0},
			want: true,
		},
	} {
		t.Run("", func(t *testing.T) {
			if got := between(test.r, test.r1, test.r2); test.want != got {
				t.Errorf("want %t, got %t", test.want, got)
			}
		})
	}
}

func Test_clearLos(t *testing.T) {
	for _, test := range []struct {
		r1, r2, r rock
		want      bool
	}{
		{
			r1:   rock{4, 0, 0},
			r2:   rock{4, 4, 0},
			r:    rock{4, 3, 0},
			want: true,
		},
		{
			r1:   rock{0, 0, 0},
			r2:   rock{10, 10, 0},
			r:    rock{2, 2, 0},
			want: true,
		},
		{
			r1:   rock{0, 0, 0},
			r2:   rock{10, 10, 0},
			r:    rock{3, 2, 0},
			want: false,
		},
	} {
		t.Run("", func(t *testing.T) {
			if got := clearLosFunc(test.r1, test.r2)(test.r); got != test.want {
				t.Errorf("want %t, got %t", test.want, got)
			}
		})
	}
}

func Test_withAngle(t *testing.T) {
	for _, test := range []struct {
		r1, r2 rock
		want   float64
	}{
		{},
		{
			r1:   rock{x: 5, y: 5},
			r2:   rock{x: 5, y: 4},
			want: 0,
		},
		{
			r1:   rock{x: 5, y: 5},
			r2:   rock{x: 6, y: 4},
			want: 45,
		},
		{
			r1:   rock{x: 5, y: 5},
			r2:   rock{x: 5, y: 6},
			want: 180,
		},
		{
			r1:   rock{x: 5, y: 5},
			r2:   rock{x: 4, y: 5},
			want: 270,
		},
		{
			r1:   rock{x: 5, y: 5},
			r2:   rock{x: 4, y: 4},
			want: 315,
		},
	} {
		t.Run("", func(t *testing.T) {
			if got := withAngle(test.r1, test.r2).angle; got != test.want {
				t.Errorf("withAngle() = %.2f, want = %.2f", got, test.want)
			}
		})
	}
}
