package main

import (
	"fmt"
	"math"
	"sort"

	"gitlab.com/DemonRax/advents2019/util"
)

func main() {
	input := util.ReadFile("input.txt")
	rock := coord200(input)
	fmt.Println(rock.x*100 + rock.y)
}

func coord200(ss []string) rock {
	all := asteroids(ss)
	_, station := maxRocks(all)

	count := 0
	for {
		var targets rocks
		targets, all = visibleWithAngles(station, all)
		sort.Sort(targets)
		for _, target := range targets {
			count++
			if count == 200 {
				return target
			}
		}
	}
	return station
}

func visibleWithAngles(station rock, all rocks) (rocks, rocks) {
	visible := make([]rock, 0, len(all))
	rest := make([]rock, 0, len(all))
	for _, rock := range all {
		if rock == station {
			continue
		}
		losFunc := clearLosFunc(station, rock)
		blocked := false
		for _, r := range all {
			if losFunc(r) {
				blocked = true
				break
			}
		}
		if !blocked {
			visible = append(visible, withAngle(station, rock))
		} else {
			rest = append(rest, rock)
		}
	}
	return visible, rest
}

func withAngle(r1, r2 rock) rock {
	angle := math.Atan2(float64(r2.x-r1.x), float64(r1.y-r2.y)) * 180 / math.Pi
	if angle < 0 {
		angle += 360
	}
	return rock{
		x:     r2.x,
		y:     r2.y,
		angle: angle,
	}
}

type rock struct {
	x, y  int
	angle float64
}

type rocks []rock

func (r rocks) Len() int {
	return len(r)
}

func (r rocks) Less(i, j int) bool {
	return r[i].angle < r[j].angle
}

func (r rocks) Swap(i, j int) {
	r[i], r[j] = r[j], r[i]
}

var _ sort.Interface = rocks{}

func station(strGrid []string) (int, rock) {
	rocks := asteroids(strGrid)
	return maxRocks(rocks)
}

func maxRocks(rocks rocks) (maxRocks int, maxRock rock) {
	for i, r1 := range rocks {
		count := 0
		for j, r2 := range rocks {
			if i == j {
				continue
			}
			losFunc := clearLosFunc(r1, r2)
			blocked := false
			for _, r := range rocks {
				if losFunc(r) {
					blocked = true
					break
				}
			}
			if !blocked {
				count++
			}
		}
		if count > maxRocks {
			maxRocks = count
			maxRock = r1
		}
	}
	return maxRocks, maxRock
}

func clearLosFunc(r1, r2 rock) func(rock) bool {
	div := float32(r2.x - r1.x)
	if div == 0 {
		return func(r rock) bool {
			return between(r, r1, r2)
		}
	}
	coef := float32(r2.y-r1.y) / div
	return func(r rock) bool {
		return between(r, r1, r2) && float32(r.y-r1.y) == float32(r.x-r1.x)*coef
	}
}

func between(r, r1, r2 rock) bool {
	if r == r1 || r == r2 {
		return false
	}
	var xIn, yIn bool
	if r2.x > r1.x {
		xIn = r.x <= r2.x && r.x >= r1.x
	} else {
		xIn = r.x >= r2.x && r.x <= r1.x
	}
	if r2.y > r1.y {
		yIn = r.y <= r2.y && r.y >= r1.y
	} else {
		yIn = r.y >= r2.y && r.y <= r1.y
	}
	return xIn && yIn
}

func asteroids(grid []string) rocks {
	res := make([]rock, 0, len(grid)*len(grid[0]))
	for y, row := range grid {
		for x, c := range row {
			if c == ('#') {
				res = append(res, rock{x, y, 0})
			}
		}
	}
	return res
}

func grid(grid []string) [][]bool {
	res := make([][]bool, len(grid))
	for y, row := range grid {
		res[y] = make([]bool, len(row))
		for x, c := range row {
			fmt.Println(x, y, c)
			res[y][x] = c == ('#')
		}
	}
	return res
}
