package main

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/DemonRax/advents2019/util"
)

func main() {
	input := util.ReadFile("input.txt")
	fmt.Println(energy(input, 1000))
}

func energy(ss []string, steps int) int {
	moons := getMoons(ss)
	for i := 0; i < steps; i++ {
		moons.move()
	}
	return moons.energy()
}

type moon struct {
	x, y, z, dx, dy, dz int
}

type moons []moon

func (ms moons) move() {
	for i, m := range ms {
		for j, n := range ms {
			if i == j {
				continue
			}
			if m.x < n.x {
				ms[i].dx++
			} else if m.x > n.x {
				ms[i].dx--
			}
			if m.y < n.y {
				ms[i].dy++
			} else if m.y > n.y {
				ms[i].dy--
			}
			if m.z < n.z {
				ms[i].dz++
			} else if m.z > n.z {
				ms[i].dz--
			}
		}
	}
	for i, m := range ms {
		m.x += m.dx
		m.y += m.dy
		m.z += m.dz
		ms[i] = m
	}
}

func (ms moons) energy() int {
	res := 0
	for _, m := range ms {
		res += (util.Abs(m.x) + util.Abs(m.y) + util.Abs(m.z)) * (util.Abs(m.dx) + util.Abs(m.dy) + util.Abs(m.dz))
	}
	return res
}

func getMoons(ss []string) moons {
	res := make([]moon, len(ss))
	for i, s := range ss {
		res[i] = parseMoon(s)
	}
	return res
}

func parseMoon(s string) (res moon) {
	coords := strings.Split(s, ",")
	res.x, _ = strconv.Atoi(strings.Split(coords[0], "=")[1])
	res.y, _ = strconv.Atoi(strings.Split(coords[1], "=")[1])
	res.z, _ = strconv.Atoi(strings.Split(strings.TrimSuffix(coords[2], ">"), "=")[1])
	return res
}
