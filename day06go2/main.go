package main

import (
	"fmt"
	"strings"

	"gitlab.com/DemonRax/advents2019/util"
)

func main() {
	input := util.ReadFile("input.txt")
	fmt.Println(jumps(input))
}

func jumps(ss []string) int {
	orbits := generateReverseOrbits(ss)
	return fromTo("YOU", "SAN", orbits)
}

func fromTo(a, b string, orbits map[string]string) int {
	toA := pathTo(a, orbits)
	toB := pathTo(b, orbits)

	for i, nodeA := range toA {
		for j, nodeB := range toB {
			if nodeA == nodeB {
				return i + j
			}
		}
	}
	return 0
}

func pathTo(to string, orbits map[string]string) []string {
	var res []string
	for {
		from, ok := orbits[to]
		if !ok {
			break
		}
		res = append(res, from)
		to = from
	}
	return res
}

func generateReverseOrbits(ss []string) map[string]string {
	res := make(map[string]string, len(ss))
	for _, s := range ss {
		ab := strings.Split(s, ")")
		res[ab[1]] = ab[0]
	}
	return res
}
