package main

import (
	"testing"

	"gitlab.com/DemonRax/advents2019/util"
)

func Test_code(t *testing.T) {
	for _, test := range []struct {
		in   string
		want int64
	}{
		{
			in: `<x=-1, y=0, z=2>
<x=2, y=-10, z=-7>
<x=4, y=-8, z=8>
<x=3, y=5, z=-1>
`,
			want: 2772,
		},
		{
			in: `<x=-8, y=-10, z=0>
<x=5, y=5, z=10>
<x=2, y=-7, z=3>
<x=9, y=-8, z=-3>
`,
			want: 4686774924,
		},
	} {
		t.Run("", func(t *testing.T) {
			if got := loop(util.ReadString(test.in)); got != test.want {
				t.Errorf("got = %d, want %d", got, test.want)
			}
		})
	}
}

func Test_parseMoon(t *testing.T) {
	for _, test := range []struct {
		in   string
		want moon
	}{
		{
			in:   "<x=-1, y=1, z=-2>",
			want: moon{x: -1, y: 1, z: -2},
		},
	} {
		t.Run(test.in, func(t *testing.T) {
			if got := parseMoon(test.in); got != test.want {
				t.Errorf("parseMoon() = %v, want = %v", got, test.want)
			}
		})
	}
}
