package main

import (
	"fmt"
	"strconv"
	"strings"
	"sync"

	"gitlab.com/DemonRax/advents2019/util"
)

func main() {
	input := util.ReadFile("input.txt")
	fmt.Println(loop(input))
}

func loop(ss []string) int64 {
	ms := getMoons(ss)
	xMoons := make([]moon, len(ms))
	yMoons := make([]moon, len(ms))
	zMoons := make([]moon, len(ms))

	for i, m := range ms {
		xMoons[i] = moon{x: m.x}
		yMoons[i] = moon{y: m.y}
		zMoons[i] = moon{z: m.z}
	}

	var x, y, z int64
	var wg sync.WaitGroup

	wg.Add(1)
	go func() {
		x = getLoop(xMoons)
		wg.Done()
	}()
	wg.Add(1)
	go func() {
		y = getLoop(yMoons)
		wg.Done()
	}()
	wg.Add(1)
	go func() {
		z = getLoop(zMoons)
		wg.Done()
	}()

	wg.Wait()
	return lcm64(x, y, z)
}

func getLoop(ms moons) int64 {
	om := make([]moon, len(ms))
	for i := range ms {
		om[i] = ms[i]
	}
	var count int64
	for {
		count++
		ms.move()
		if ms.equal(om) {
			break
		}
	}
	return count
}

func lcm64(a, b, c int64) int64 {
	lcm := a * (b / gcd64(a, b))
	return c * (lcm / gcd64(lcm, c))
}

func gcd64(m, n int64) int64 {
	if n == 0 {
		return m
	}
	return gcd64(n, m%n)
}

type moon struct {
	x, y, z, dx, dy, dz int
}

func (m moon) equal(n moon) bool {
	return m.x == n.x &&
		m.y == n.y &&
		m.z == n.z &&
		m.dx == n.dx &&
		m.dy == n.dy &&
		m.dz == n.dz
}

type moons []moon

func (ms moons) move() {
	for i, m := range ms {
		for j, n := range ms {
			if i == j {
				continue
			}
			if m.x < n.x {
				ms[i].dx++
			} else if m.x > n.x {
				ms[i].dx--
			}
			if m.y < n.y {
				ms[i].dy++
			} else if m.y > n.y {
				ms[i].dy--
			}
			if m.z < n.z {
				ms[i].dz++
			} else if m.z > n.z {
				ms[i].dz--
			}
		}
	}
	for i, m := range ms {
		m.x += m.dx
		m.y += m.dy
		m.z += m.dz
		ms[i] = m
	}
}

func (ms moons) equal(om moons) bool {
	for i, m := range ms {
		if !m.equal(om[i]) {
			return false
		}
	}
	return true
}

func getMoons(ss []string) moons {
	res := make([]moon, len(ss))
	for i, s := range ss {
		res[i] = parseMoon(s)
	}
	return res
}

func parseMoon(s string) (res moon) {
	coords := strings.Split(s, ",")
	res.x, _ = strconv.Atoi(strings.Split(coords[0], "=")[1])
	res.y, _ = strconv.Atoi(strings.Split(coords[1], "=")[1])
	res.z, _ = strconv.Atoi(strings.Split(strings.TrimSuffix(coords[2], ">"), "=")[1])
	return res
}
