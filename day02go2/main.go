package main

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/DemonRax/advents2019/util"
)

func main() {
	input := util.ReadFile("input.txt")
	n, v := getNounVerb(input[0])
	fmt.Println(n*100 + v)
}

func getNounVerb(s string) (int, int) {
	for n := 0; n < 100; n++ {
		for v := 0; v < 100; v++ {
			res := intcode(s, map[int]int{1: n, 2: v})
			strs := strings.Split(res, ",")
			resInt, _ := strconv.Atoi(strs[0])
			if resInt == 19690720 {
				return n, v
			}
		}
	}
	return 0, 0
}

func intcode(s string, replace map[int]int) string {
	codes := strings.Split(s, ",")
	ints := make([]int, len(codes))
	for i, c := range codes {
		ints[i], _ = strconv.Atoi(c)
	}
	for i, v := range replace {
		ints[i] = v
	}
	i := 0
	for {
		if ints[i] == 99 {
			break
		}
		if ints[i] == 1 {
			ints[ints[i+3]] = ints[ints[i+1]] + ints[ints[i+2]]
		} else if ints[i] == 2 {
			ints[ints[i+3]] = ints[ints[i+1]] * ints[ints[i+2]]
		}
		i += 4
	}
	for i := range ints {
		codes[i] = strconv.Itoa(ints[i])
	}
	return strings.Join(codes, ",")
}
