package main

import (
	"testing"
)

func Test_code(t *testing.T) {
	for _, test := range []struct {
		in   string
		width,
		height int
		want string
	}{
		{
			in:   "0222112222120000",
			width: 2,
			height: 2,
			want: "0110",
		},
	} {
		t.Run(test.in, func(t *testing.T) {
			if got := picture(test.in, test.width, test.height); got != test.want {
				t.Errorf("got = %s, want %s", got, test.want)
			}
		})
	}
}
