package main

import (
	"fmt"

	"gitlab.com/DemonRax/advents2019/util"
)

func main() {
	input := util.ReadFile("input.txt")
	fmt.Println(picture(input[0], 25, 6))
}

func picture(s string, width, height int) string {
	res := make([]uint8, width*height)
	for i := range res {
		res[i] = '2'
	}
	var c int
	for c < len(s) {
		var i int
		for x := 0; x < width; x++ {
			for y := 0; y < height; y++ {
				if res[i] == '2' && (s[c] == '0' || s[c] == '1') {
					res[i] = s[c]
				}
				c++
				i++
			}
		}
	}
	return string(res)
}