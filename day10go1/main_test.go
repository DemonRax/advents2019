package main

import (
	"testing"

	"gitlab.com/DemonRax/advents2019/util"
)

func Test_code(t *testing.T) {
	for _, test := range []struct {
		in   string
		want int
	}{
		{
			in:   `.###`,
			want: 2,
		},
		{
			in: `.###
..#.
..#.`,
			want: 4,
		},
		{
			in: `.#..#
.....
#####
....#
...##
`,
			want: 8,
		},
		{
			in: `......#.#.
#..#.#....
..#######.
.#.#.###..
.#..#.....
..#....#.#
#..#....#.
.##.#..###
##...#..#.
.#....####
`,
			want: 33,
		},
		{
			in: `#.#...#.#.
.###....#.
.#....#...
##.#.#.#.#
....#.#.#.
.##..###.#
..#...##..
..##....##
......#...
.####.###.
`,
			want: 35,
		},
		{
			in: `.#..#..###
####.###.#
....###.#.
..###.##.#
##.##.#.#.
....###..#
..#.#..#.#
#..#.#.###
.##...##.#
.....#.#..
`,
			want: 41,
		},
		{
			in: `.#..##.###...#######
##.############..##.
.#.######.########.#
.###.#######.####.#.
#####.##.#.##.###.##
..#####..#.#########
####################
#.####....###.#.#.##
##.#################
#####.##.###..####..
..######..##.#######
####.##.####...##..#
.#####..#.######.###
##...#.##########...
#.##########.#######
.####.#.###.###.#.##
....##.##.###..#####
.#.#.###########.###
#.#.#.#####.####.###
###.##.####.##.#..##
`,
			want: 210,
		},
	} {
		t.Run(test.in[:4], func(t *testing.T) {
			if got := station(util.ReadString(test.in)); got != test.want {
				t.Errorf("got = %d, want %d", got, test.want)
			}
		})
	}
}

func Test_between(t *testing.T) {
	for _, test := range []struct {
		r, r1, r2 rock
		want      bool
	}{
		{
			r:    rock{5, 6},
			r1:   rock{1, 1},
			r2:   rock{10, 10},
			want: true,
		},
		{
			r:    rock{5, 6},
			r1:   rock{5, 6},
			r2:   rock{10, 10},
			want: false,
		},
		{
			r:    rock{5, 6},
			r1:   rock{10, 10},
			r2:   rock{10, 10},
			want: false,
		},
		{
			r:    rock{10, 10},
			r1:   rock{10, 10},
			r2:   rock{10, 10},
			want: false,
		},
		{
			r:    rock{10, 10},
			r1:   rock{5, 6},
			r2:   rock{10, 10},
			want: false,
		},
		{
			r:    rock{5, 6},
			r1:   rock{10, 10},
			r2:   rock{1, 1},
			want: true,
		},
		{
			r:    rock{1, 1},
			r1:   rock{10, 10},
			r2:   rock{5, 6},
			want: false,
		},
		{
			r:    rock{5, 6},
			r1:   rock{5, 1},
			r2:   rock{5, 10},
			want: true,
		},
	} {
		t.Run("", func(t *testing.T) {
			if got := between(test.r, test.r1, test.r2); test.want != got {
				t.Errorf("want %t, got %t", test.want, got)
			}
		})
	}
}

func Test_clearLos(t *testing.T) {
	for _, test := range []struct {
		r1, r2, r rock
		want      bool
	}{
		{
			r1:   rock{4, 0},
			r2:   rock{4, 4},
			r:    rock{4, 3},
			want: true,
		},
		{
			r1:   rock{0, 0},
			r2:   rock{10, 10},
			r:    rock{2, 2},
			want: true,
		},
		{
			r1:   rock{0, 0},
			r2:   rock{10, 10},
			r:    rock{3, 2},
			want: false,
		},
	} {
		t.Run("", func(t *testing.T) {
			if got := clearLosFunc(test.r1, test.r2)(test.r); got != test.want {
				t.Errorf("want %t, got %t", test.want, got)
			}
		})
	}
}
