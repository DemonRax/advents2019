package main

import (
	"fmt"

	"gitlab.com/DemonRax/advents2019/util"
)

func main() {
	input := util.ReadFile("input.txt")
	fmt.Println(station(input))
}

type rock struct {
	x, y int
}

func station(strGrid []string) int {
	rocks := asteroids(strGrid)
	maxRock, maxRocks := rock{}, 0
	for i, r1 := range rocks {
		count := 0
		for j, r2 := range rocks {
			if i == j {
				continue
			}
			losFunc := clearLosFunc(r1, r2)
			blocked := false
			for _, r := range rocks {
				if losFunc(r) {
					blocked = true
					break
				}
			}
			if !blocked {
				count++
			}
		}
		if count > maxRocks {
			maxRocks = count
			maxRock = r1
		}
	}
	fmt.Println(maxRock)
	return maxRocks
}

func clearLosFunc(r1, r2 rock) func(rock) bool {
	div := float32(r2.x - r1.x)
	if div == 0 {
		return func(r rock) bool {
			return between(r, r1, r2)
		}
	}
	coef := float32(r2.y-r1.y) / div
	return func(r rock) bool {
		return between(r, r1, r2) && float32(r.y-r1.y) == float32(r.x-r1.x)*coef
	}
}

func between(r, r1, r2 rock) bool {
	if r == r1 || r == r2 {
		return false
	}
	var xIn, yIn bool
	if r2.x > r1.x {
		xIn = r.x <= r2.x && r.x >= r1.x
	} else {
		xIn = r.x >= r2.x && r.x <= r1.x
	}
	if r2.y > r1.y {
		yIn = r.y <= r2.y && r.y >= r1.y
	} else {
		yIn = r.y >= r2.y && r.y <= r1.y
	}
	return xIn && yIn
}

func asteroids(grid []string) []rock {
	res := make([]rock, 0, len(grid)*len(grid[0]))
	for y, row := range grid {
		for x, c := range row {
			if c == ('#') {
				res = append(res, rock{x, y})
			}
		}
	}
	return res
}

func grid(grid []string) [][]bool {
	res := make([][]bool, len(grid))
	for y, row := range grid {
		res[y] = make([]bool, len(row))
		for x, c := range row {
			fmt.Println(x, y, c)
			res[y][x] = c == ('#')
		}
	}
	return res
}
