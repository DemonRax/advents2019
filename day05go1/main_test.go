package main

import (
	"strconv"
	"testing"
)

func Test_code(t *testing.T) {
	for _, test := range []struct {
		in string
		input,
		want int
	}{
		{
			in:   "4,0,99",
			want: 4,
		},
		{
			in:    "3,0,4,0,99",
			input: 15,
			want:  15,
		},
		{
			in:    "3,9,1002,9,3,9,4,9,99,0",
			input: 33,
			want:  99,
		},
		{
			in:    "3,3,1101,0,-1,4,4,4,99",
			input: 100,
			want:  99,
		},
	} {
		t.Run(test.in, func(t *testing.T) {
			if got := intcode(test.in, test.input); got != test.want {
				t.Errorf("got = %d, want %d", got, test.want)
			}
		})
	}
}

func Test_modes(t *testing.T) {
	for _, test := range []struct {
		in,
		wantOp,
		want1,
		want2,
		want3 int
	}{
		{
			in:     11101,
			wantOp: 1,
			want1:  1,
			want2:  1,
			want3:  1,
		},
		{
			in:     1102,
			wantOp: 2,
			want1:  1,
			want2:  1,
			want3:  0,
		},
		{
			in:     103,
			wantOp: 3,
			want1:  1,
			want2:  0,
			want3:  0,
		},
		{
			in:     04,
			wantOp: 4,
			want1:  0,
			want2:  0,
			want3:  0,
		},
	} {
		t.Run(strconv.Itoa(test.in), func(t *testing.T) {
			got, got1, got2, got3 := params(test.in)
			if got != test.wantOp {
				t.Errorf("got = %d, want %d", got, test.wantOp)
			}
			if got != test.wantOp {
				t.Errorf("got = %d, want %d", got, test.wantOp)
			}
			if got1 != test.want1 {
				t.Errorf("got = %d, want %d", got2, test.want1)
			}
			if got2 != test.want2 {
				t.Errorf("got = %d, want %d", got2, test.want2)
			}
			if got3 != test.want3 {
				t.Errorf("got = %d, want %d", got3, test.want3)
			}
		})
	}
}
