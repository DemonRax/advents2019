package main

import (
	"fmt"
	"math"
	"strconv"
	"strings"

	"gitlab.com/DemonRax/advents2019/util"
)

func main() {
	input := util.ReadFile("input.txt")
	fmt.Println(intcode(input[0], 1))
}

func intcode(s string, mem int) int {
	codes := strings.Split(s, ",")
	ints := make([]int, len(codes))
	for i, c := range codes {
		ints[i], _ = strconv.Atoi(c)
	}
	i := 0
	for {
		code, mode1, mode2 := ints[i], 0, 0
		if code == 99 {
			break
		}
		if code > 4 {
			code, mode1, mode2, _ = params(code)
		}
		var v1, v2 int
		if mode1 == 1 {
			v1 = ints[i+1]
		} else {
			v1 = ints[ints[i+1]]
		}
		if code == 1 || code == 2 {
			if mode2 == 1 {
				v2 = ints[i+2]
			} else {
				v2 = ints[ints[i+2]]
			}
		}
		if code == 1 {
			ints[ints[i+3]] = v1 + v2
			i += 4
		} else if code == 2 {
			ints[ints[i+3]] = v1 * v2
			i += 4
		} else if code == 3 {
			ints[ints[i+1]] = mem
			i += 2
		} else if code == 4 {
			mem = v1
			i += 2
		}
	}
	for i := range ints {
		codes[i] = strconv.Itoa(ints[i])
	}
	//return strings.Join(codes, ",")
	return mem
}

func params(code int) (op, mode1, mode2, mode3 int) {
	return digit(code, 1), digit(code, 3), digit(code, 4), digit(code, 5)
}

func digit(num, place int) int {
	r := num % int(math.Pow(10, float64(place)))
	return r / int(math.Pow(10, float64(place-1)))
}
