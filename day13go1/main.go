package main

import (
	"fmt"
	"gitlab.com/DemonRax/advents2019/util"
)

func main() {
	input := util.ReadFile("input.txt")
	fmt.Println(blocks(generateGame(input[0])))
}

func generateGame(s string) (res []int) {
	ic := util.NewComputer(s, 0, 0)
	for ic.IsRunning() {
		ic.Run(true)
		res = append(res, ic.Memory())
	}
	return res
}

func blocks(in []int) int {
	count := 0
	for i := 2; i < len(in)-3; i += 3 {
		if in[i] == 2 {
			count++
		}
	}
	return count
}
