package main

import (
	"testing"
)

func Test_code(t *testing.T) {
	for _, test := range []struct {
		in   []int
		want int
	}{
		{
			in:   []int{1, 2, 3, 6, 5, 4},
			want: 1,
		},
	} {
		t.Run("", func(t *testing.T) {
			if got := blocks(test.in); got != test.want {
				t.Errorf("got = %d, want %d", got, test.want)
			}
		})
	}
}
