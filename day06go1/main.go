package main

import (
	"fmt"
	"strings"

	"gitlab.com/DemonRax/advents2019/util"
)

func main() {
	input := util.ReadFile("input.txt")
	fmt.Println(countOrbits(input))
}

func countOrbits(ss []string) int {
	orbits := generateOrbits(ss)
	return count(orbits)
}

func generateOrbits(ss []string) map[string][]string {
	res := make(map[string][]string, len(ss))
	for _, s := range ss {
		ab := strings.Split(s, ")")
		res[ab[0]] = append(res[ab[0]], ab[1])
	}
	return res
}

func count(orbits map[string][]string) int {
	var res int
	for _, orbit := range orbits {
		res += countOne(orbit, orbits)
	}
	return res
}

func countOne(orbit []string, orbits map[string][]string) int {
	res := len(orbit)
	for _, o := range orbit {
		res += countOne(orbits[o], orbits)
	}
	return res
}
