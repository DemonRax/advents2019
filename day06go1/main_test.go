package main

import (
	"testing"
)

func Test_code(t *testing.T) {
	for _, test := range []struct {
		in   []string
		want int
	}{
		{
			in: []string{
				"COM)B",
				"B)C",
				"C)D",
				"D)E",
				"E)F",
				"B)G",
				"G)H",
				"D)I",
				"E)J",
				"J)K",
				"K)L",
			},
			want: 42,
		},
	} {
		t.Run(test.in[0], func(t *testing.T) {
			if got := countOrbits(test.in); got != test.want {
				t.Errorf("got = %d, want %d", got, test.want)
			}
		})
	}
}
