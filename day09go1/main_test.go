package main

import (
	"testing"
)

func Test_code(t *testing.T) {
	for _, test := range []struct {
		in   string
		want int
	}{
		{
			in:   "109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99",
			want: 99,
		},
		{
			in:   "1102,34915192,34915192,7,4,7,99,0",
			want: 1219070632396864,
		},
		{
			in:   "104,1125899906842624,99",
			want: 1125899906842624,
		},
	} {
		t.Run(test.in, func(t *testing.T) {
			if got := intcode(test.in, 0); got != test.want {
				t.Errorf("got = %d, want %d", got, test.want)
			}
		})
	}
}