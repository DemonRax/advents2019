package main

import (
	"fmt"

	"gitlab.com/DemonRax/advents2019/util"
)

func main() {
	input := util.ReadFile("input.txt")
	fmt.Println(intcode(input[0], 1))
}

func intcode(s string, input int) int {
	ic := util.NewComputer(s, input, 1)
	ic.Run(false)
	return ic.Memory()
}
