package main

import (
	"fmt"
	"strconv"

	"gitlab.com/DemonRax/advents2019/util"
)

func main() {
	input := util.ReadFile("input.txt")
	result := 0
	for _, in := range input {
		result += fuel(in)
	}
	fmt.Println(result)
}

func fuel(s string) int {
	f, _ := strconv.Atoi(s)
	f /= 3
	f -= 2
	return f
}
