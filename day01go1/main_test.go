package main

import "testing"

func Test_code(t *testing.T) {
	for _, test := range []struct {
		in   string
		want int
	}{
		{
			in:   "12",
			want: 2,
		},
		{
			in:   "14",
			want: 2,
		},
		{
			in:   "1969",
			want: 654,
		},
		{
			in:   "100756",
			want: 33583,
		},
	} {
		t.Run(test.in, func(t *testing.T) {
			if got := fuel(test.in); got != test.want {
				t.Errorf("got = %d, want %d", got, test.want)
			}
		})
	}
}
